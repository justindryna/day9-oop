<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new Animal("Sheep");
echo "Name : $object->name <br>";
echo "Legs : $object->legs <br>";
echo "Cold Bloode : $object->cold_blooded <br><br>";


$frog = new Frog("Buduk");
echo "Name : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "Cold Bloode : $frog->cold_blooded <br>";
echo "Jump : $frog->jump <br><br>";


$ape = new Ape("Kera Sakti");
echo "Name : $ape->name <br>";
echo "Legs : $ape->legs <br>";
echo "Cold Bloode : $ape->cold_blooded <br>";
echo "Yell : $ape->yell <br>";